package Interfaz;

import java.awt.Dimension;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import Jugadores.DatoJugador;
import Presentador.InteraccionDeClases;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.util.ArrayList;

public class Pantalla {

	private JFrame frmCreadorDeEquipos;
	private InteraccionDeClases presentador=new InteraccionDeClases();
	private JTable table_2;;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Pantalla window = new Pantalla();
					window.frmCreadorDeEquipos.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Pantalla() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmCreadorDeEquipos = new JFrame();
		frmCreadorDeEquipos.setTitle("CREADOR DE EQUIPOS IDEALES");
		frmCreadorDeEquipos.setBounds(100, 100, 450, 300);
		frmCreadorDeEquipos.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmCreadorDeEquipos.getContentPane().setLayout(null);
		
		final JButton btnNewButton = new JButton("Armar Equipo Ideal");
		btnNewButton.setEnabled(false);
		btnNewButton.setBounds(137, 124, 181, 40);
		btnNewButton.setVisible(false);
		frmCreadorDeEquipos.getContentPane().add(btnNewButton);
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				JFrame ventana=new JFrame();
				ventana.setTitle("Equipo Ideal");
				ventana.setSize(700,200);
				ventana.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
				ventana.setVisible(true);
					
				String[] cabezera={"nombre","pais origen", "posicion",
						"puntaje total","cant goles","cant tarjetas","cant faltas"};
				//presentador=new InteraccionDeClases();
				ArrayList<DatoJugador>ideal=presentador.dameEquipoIdeal();
				Object[][] valores=new Object[ideal.size()][cabezera.length];
				System.out.println(ideal.size());
				double puntajePromedio=0;
				for(int i=0;i<ideal.size();i++){
					inicializarDatosIdeal(valores, i,ideal);			
					puntajePromedio+=ideal.get(i).getPuntajePromedio();
				}
				puntajePromedio=puntajePromedio/ideal.size();
				
				table_2 = new JTable(valores,cabezera);
				JScrollPane js=new JScrollPane(table_2);
				js.setPreferredSize(new Dimension(400,150));
				ventana.add(js);
				
				JOptionPane.showMessageDialog(ventana,puntajePromedio+"",
					    "puntaje promedio",JOptionPane.PLAIN_MESSAGE);
			}
			
			

			private void inicializarDatosIdeal(Object[][] valores1, int i,ArrayList<DatoJugador>ideal) {
				// TODO Auto-generated method stub
				valores1[i][0]=ideal.get(i).nombre.toLowerCase();
				valores1[i][1]=ideal.get(i).getPaisOrigen().toLowerCase();
				valores1[i][2]=ideal.get(i).getPosicionEnCancha()[0].toLowerCase();
				valores1[i][3]=ideal.get(i).getPuntajePromedio();
				valores1[i][4]=ideal.get(i).getGoles();
				valores1[i][5]=ideal.get(i).getCantTarjetas();
				valores1[i][6]=ideal.get(i).getCantFaltas();
			}
			
		});
		
		final JButton btnNewButton_1 = new JButton("Agregar Jugador");
		btnNewButton_1.setEnabled(false);
		btnNewButton_1.setVisible(false);
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				boolean activarError=false;
				Integer seleccion=null,seleccionSegunda=null;
				String paisOrigen,puntajeConsolidado=null,cantidadDeGoles=null,cantidadTarjetas = null,cantidadFaltas=null;
				
				
				String nombre =JOptionPane.showInputDialog(btnNewButton_1, "Dime el nombre del jugador",JOptionPane.MESSAGE_PROPERTY);
				if(nombre!=null){paisOrigen =JOptionPane.showInputDialog(btnNewButton_1, "Dime el pais de origen",JOptionPane.MESSAGE_PROPERTY);
		
					if(paisOrigen!=null){seleccion = JOptionPane.showOptionDialog(btnNewButton_1, "Seleccione una posicion en la cancha", 
							"Selector de posicon",JOptionPane.YES_NO_CANCEL_OPTION,JOptionPane.QUESTION_MESSAGE,
							null,presentador.getPosicionesJugador(),null);		
				
						if(seleccion!=null){ seleccionSegunda = JOptionPane.showOptionDialog( btnNewButton_1, "Seleccione una segunda posicion en la cancha", 
								"Selector de posicion", JOptionPane.YES_NO_CANCEL_OPTION,JOptionPane.QUESTION_MESSAGE,
								null, presentador.getPosicionesJugador(), null);
				
							if(seleccionSegunda!=null){ puntajeConsolidado =JOptionPane.showInputDialog(btnNewButton_1, "Dime el puntaje total en el torneo",JOptionPane.MESSAGE_PROPERTY);
				
								if(puntajeConsolidado!=null && esDouble(puntajeConsolidado)){ cantidadDeGoles =JOptionPane.showInputDialog(btnNewButton_1, "Dime la cantidad de goles que metio",JOptionPane.MESSAGE_PROPERTY);

									if(cantidadDeGoles!=null && esInteger(cantidadDeGoles)){ cantidadTarjetas =JOptionPane.showInputDialog(btnNewButton_1, "Dime la cantidad de tarjetas que le sacaron",JOptionPane.MESSAGE_PROPERTY);
				
										if(cantidadTarjetas!=null && esInteger(cantidadTarjetas)){ cantidadFaltas =JOptionPane.showInputDialog(btnNewButton_1, "Dime la cantidad de faltas que realizo",JOptionPane.MESSAGE_PROPERTY);
										}else activarError=true;
									}else activarError=true;
								}else activarError=true;
							}
						}
					}
					if(activarError==true || Integer.parseInt(cantidadTarjetas)<0 || Integer.parseInt(cantidadFaltas)<0 ||
							Integer.parseInt(cantidadDeGoles)<0 || Double.parseDouble(puntajeConsolidado)<0){
								JOptionPane.showMessageDialog(table_2,"algun dato incorrecto(posible numero negativo o ingreso "
								+ "una letra cuando no correspondia)",
								"ERROR: INTENTELO NUEVAMENTE",JOptionPane.ERROR_MESSAGE);
					}else{
						presentador.agregarJugador(nombre,presentador.getPosicionesJugador()[seleccion],presentador.getPosicionesJugador()[seleccionSegunda], 
						Double.parseDouble(puntajeConsolidado),Integer.parseInt(cantidadDeGoles),
						Integer.parseInt(cantidadTarjetas),Integer.parseInt(cantidadFaltas),paisOrigen);			
					}
				}
			}

			private boolean esDouble(String cadena) {
				boolean resultado=false;

		        try {
		            Double.parseDouble(cadena);
		            resultado = true;
		        } catch (NumberFormatException excepcion) {
		            resultado = false;
		        }

		        return resultado;
			}
			private boolean esInteger(String cadena) {

		        boolean resultado=false;

		        try {
		            Double.parseDouble(cadena);
		            resultado = true;
		        } catch (NumberFormatException excepcion) {
		            resultado = false;
		        }

		        return resultado;
		    }
		});
		
		btnNewButton_1.setBounds(137, 62, 181, 40);
		frmCreadorDeEquipos.getContentPane().add(btnNewButton_1);
		
		final JButton btnNewButton_3 = new JButton("ver jugadores ingresados");
		btnNewButton_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				JFrame ventana=new JFrame();
				ventana.setTitle("Jugadores Ingresados");
				ventana.setSize(700,200);
				ventana.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
				ventana.setVisible(true);
					
				
				String[] cabezera={"nombre","pais origen", "1ra posicion",
					"2da posicion","puntaje total","cant goles","cant tarjetas","cant faltas"};
				
				Object[][] valores=new Object[presentador.jugadoresTotales().size()][cabezera.length];
				
				
				ArrayList<DatoJugador> totales=presentador.jugadoresTotales();
				
				for(int i=0;i<presentador.jugadoresTotales().size();i++)
					inicializarDatosJugador(valores, i, totales);			
				
				table_2 = new JTable(valores,cabezera);
				JScrollPane js=new JScrollPane(table_2);
				js.setPreferredSize(new Dimension(400,150));
				ventana.add(js);
				
			}

			private void inicializarDatosJugador(Object[][] valores, int i, ArrayList<DatoJugador> totales) {
				valores[i][0]=totales.get(i).nombre.toLowerCase();
				valores[i][1]=totales.get(i).getPaisOrigen().toLowerCase();
				valores[i][2]=totales.get(i).getPosicionEnCancha()[0].toLowerCase();
				valores[i][3]=totales.get(i).getPosicionEnCancha()[0].toLowerCase();
				valores[i][4]=totales.get(i).getPuntajePromedio();
				valores[i][5]=totales.get(i).getGoles();
				valores[i][6]=totales.get(i).getCantTarjetas();
				valores[i][7]=totales.get(i).getCantFaltas();
			}
				
		});
		
		btnNewButton_3.setEnabled(false);
		btnNewButton_3.setVisible(false);
		btnNewButton_3.setBounds(26, 227, 199, 23);
		frmCreadorDeEquipos.getContentPane().add(btnNewButton_3);
		
		final JButton btnNewButton_2 = new JButton("Comienza a Jugar");
		btnNewButton_2.addActionListener(new ActionListener() {
			@SuppressWarnings("deprecation")
			public void actionPerformed(ActionEvent arg0) {
				btnNewButton_2.enable(false);
				btnNewButton_2.setVisible(false);
				btnNewButton_1.setEnabled(true);
				btnNewButton_1.setVisible(true);
				btnNewButton.setEnabled(true);
				btnNewButton.setVisible(true);
				btnNewButton_3.setEnabled(true);
				btnNewButton_3.setVisible(true);
			}
		});
		btnNewButton_2.setBounds(137, 95, 181, 40);
		frmCreadorDeEquipos.getContentPane().add(btnNewButton_2);
		
		
		
		
		
	}
	
	
			
}
