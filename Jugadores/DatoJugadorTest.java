package Jugadores;

import static org.junit.Assert.*;

import org.junit.Test;

public class DatoJugadorTest {

	@Test
	public void DatoJugadorNormalTest() {
		DatoJugador nuevo=new DatoJugador("primero","defensor izquierdo","defensor derecho",
				8.3,5,5,5,"argentina");
		
		assertTrue(nuevo.getPuntajePromedio()>8);		
	}
	
	@Test(expected= IllegalArgumentException.class)
	public void DatoJugadorConErroresTest() {
		DatoJugador nuevo=new DatoJugador("primero","defensor izquierdo","defensor derecho",
				-8.3,5,5,5,"argentina");
		
		assertTrue(nuevo.getPuntajePromedio()>0);
	}
	@Test(expected= IllegalArgumentException.class)
	public void DatoJugadorConOtroErrorTest() {
		DatoJugador nuevo=new DatoJugador("primero","defensor izquierdo","defensor derecho",
				8.3,-5,0,0,"argentina");
		
		assertTrue(nuevo.getPuntajePromedio()>0);
	}
	@Test(expected= IllegalArgumentException.class)
	public void DatoJugadorErrorNacionTest() {
		DatoJugador nuevo=new DatoJugador("primero","defensor izquierdo","defensor derecho",
				8.3,5,5,5,"1r");
		
		assertTrue(nuevo.getPuntajePromedio()>0);
	}
	@Test
	public void DatoJugadorMuchasFaltasTest() {
		DatoJugador nuevo=new DatoJugador("primero","defensor izquierdo","defensor derecho",
				4,0,15,15,"argentina");
		
		assertTrue(nuevo.getPuntajePromedio()==4.0);		
	}
}
