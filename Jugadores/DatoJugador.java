package Jugadores;

import java.util.Arrays;

public class DatoJugador {
	private String[] posicionEnCancha;
	public String nombre;
	private double puntajePromedio;
	private int goles;
	private int cantTarjetas;
	private int cantFaltas;
	private String paisOrigen;
	
	public DatoJugador(String nombre,String posCancha, String posCancha1, double puntaje,
			int goles, int tarjetas, int faltas,String paisOrigen){
		
		//el pais origen tiene que tenes 3 o mas letras o numeros
		if(puntaje<0 || goles<0 || tarjetas<0 || faltas<0 || paisOrigen.length()<3){
			 throw new IllegalArgumentException();
		}
		
		this.nombre=nombre;
		puntajePromedio=puntaje;
		this.goles=goles;
		cantTarjetas=tarjetas;
		cantFaltas=faltas;
		
		posicionEnCancha=new String[2];
		posicionEnCancha[0]=posCancha;
		posicionEnCancha[1]=posCancha1;
		
		this.paisOrigen=paisOrigen;
	}
	
	public double obtenerCoeficiente(){
		return (2*getGoles())-((getCantFaltas()/10)-getCantTarjetas())+getPuntajePromedio();
	}
	public String getNombre(){
		return nombre;
	}

	public String[] getPosicionEnCancha() {
		return posicionEnCancha;
	}

	public double getPuntajePromedio() {
		return puntajePromedio;
	}

	public int getGoles() {
		return goles;
	}

	public int getCantTarjetas() {
		return cantTarjetas;
	}

	public int getCantFaltas() {
		return cantFaltas;
	}
	public String getPaisOrigen(){
		return paisOrigen;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + cantFaltas;
		result = prime * result + cantTarjetas;
		result = prime * result + goles;
		result = prime * result + ((nombre == null) ? 0 : nombre.hashCode());
		result = prime * result
				+ ((paisOrigen == null) ? 0 : paisOrigen.hashCode());
		result = prime * result + Arrays.hashCode(posicionEnCancha);
		long temp;
		temp = Double.doubleToLongBits(puntajePromedio);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DatoJugador other = (DatoJugador) obj;
		if (cantFaltas != other.cantFaltas)
			return false;
		if (cantTarjetas != other.cantTarjetas)
			return false;
		if (goles != other.goles)
			return false;
		if (nombre == null) {
			if (other.nombre != null)
				return false;
		} else if (!nombre.equals(other.nombre))
			return false;
		if (paisOrigen == null) {
			if (other.paisOrigen != null)
				return false;
		} else if (!paisOrigen.equals(other.paisOrigen))
			return false;
		if (!Arrays.equals(posicionEnCancha, other.posicionEnCancha))
			return false;
		if (Double.doubleToLongBits(puntajePromedio) != Double
				.doubleToLongBits(other.puntajePromedio))
			return false;
		return true;
	}

	
}
