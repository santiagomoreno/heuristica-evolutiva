package Jugadores;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class JugadoresDelMundialTest {

	
	@Test
	public void MundialCantJugadorestest() {
		JugadoresDelMundial jugadores=new JugadoresDelMundial();
		DatoJugador nuevo=new DatoJugador("primero","defensor izquierdo","defensor derecho",8.3,5,5,5,"argentina");
		DatoJugador nuevo1=new DatoJugador("segundo","defensor izquierdo","defensor derecho",6.3,7,1,3,"peru");
		jugadores.agregarJugador(nuevo);
		jugadores.agregarJugador(nuevo1);
		
		assertTrue(jugadores.cantJugadores()==2);
	}
	@Test
	public void MundialCantJugadoresCerotest() {
		JugadoresDelMundial jugadores=new JugadoresDelMundial();
		assertTrue(jugadores.cantJugadores()==0);
	}
	@Test
	public void MundialCantJugadoresRepetidostest() {
		JugadoresDelMundial jugadores=new JugadoresDelMundial();
		DatoJugador nuevo=new DatoJugador("primero","defensor izquierdo","defensor derecho",8.3,5,5,5,"argentina");
		jugadores.agregarJugador(nuevo);
		jugadores.agregarJugador(nuevo);
		
		assertTrue(jugadores.cantJugadores()==1);
	}
	@Test
	public void MundialCantJugadoresRepetidosBooleantest() {
		JugadoresDelMundial jugadores=new JugadoresDelMundial();
		DatoJugador nuevo=new DatoJugador("primero","defensor izquierdo","defensor derecho",8.3,5,5,5,"argentina");
		jugadores.agregarJugador(nuevo);
		
		assertTrue(jugadores.agregarJugador(nuevo)==false);
	}
	@Test
	public void MundialTraerNombretest() {
		JugadoresDelMundial jugadores=new JugadoresDelMundial();
		DatoJugador nuevo=new DatoJugador("primero","defensor izquierdo","defensor derecho",8.3,5,5,5,"argentina");
		jugadores.agregarJugador(nuevo);
		
		assertTrue(jugadores.getJugador(0).getNombre().equals("primero"));
	}
	@Test
	public void MundialTraerNombreConMastest() {
		JugadoresDelMundial jugadores=new JugadoresDelMundial();
		DatoJugador nuevo=new DatoJugador("primero","defensor izquierdo","defensor derecho",8.3,5,5,5,"argentina");
		DatoJugador nuevo1=new DatoJugador("segundo","mirame","defensor derecho",8.3,5,5,5,"argentina");
		jugadores.agregarJugador(nuevo);
		jugadores.agregarJugador(nuevo1);
		
		
		assertTrue(jugadores.getJugador(1).getPosicionEnCancha()[0].equals("mirame"));
	}
	@Test(expected=IllegalArgumentException.class)
	public void MundialTraerNombreIncorrectotest() {
		JugadoresDelMundial jugadores=new JugadoresDelMundial();
		DatoJugador nuevo=new DatoJugador("primero","defensor izquierdo","defensor derecho",8.3,5,5,5,"argentina");
		jugadores.agregarJugador(nuevo);
		
		assertFalse(jugadores.getJugador(1).getNombre().equals("nose"));
	}
	@Test(expected=IllegalArgumentException.class)
	public void MundialTraerNombreNegativotest() {
		JugadoresDelMundial jugadores=new JugadoresDelMundial();
		DatoJugador nuevo=new DatoJugador("primero","defensor izquierdo","defensor derecho",8.3,5,5,5,"argentina");
		jugadores.agregarJugador(nuevo);
		
		assertFalse(jugadores.getJugador(-1).getNombre().equals("nose"));
	}
	@Test
	public void MundialTraerJugadoresEnPostest() {
		JugadoresDelMundial jugadores=new JugadoresDelMundial();
		DatoJugador nuevo=new DatoJugador("primero","defensor izquierdo","defensor derecho",8.3,5,5,5,"argentina");
		DatoJugador nuevo1=new DatoJugador("segundo","mirame","defensor derecho",8.3,5,5,5,"argentina");
		DatoJugador nuevo2=new DatoJugador("tercero","mirame","defensor derecho",8.3,5,5,5,"argentina");
		jugadores.agregarJugador(nuevo);
		jugadores.agregarJugador(nuevo1);
		jugadores.agregarJugador(nuevo2);
		
		assertTrue(jugadores.jugadoresEnPos("mirame").contains(nuevo1));
	}
	@Test
	public void MundialTraerJugadoresEnPosSizetest() {
		JugadoresDelMundial jugadores=new JugadoresDelMundial();
		DatoJugador nuevo=new DatoJugador("primero","defensor izquierdo","defensor derecho",8.3,5,5,5,"argentina");
		DatoJugador nuevo1=new DatoJugador("segundo","mirame","defensor derecho",8.3,5,5,5,"argentina");
		DatoJugador nuevo2=new DatoJugador("tercero","mirame","defensor derecho",8.3,5,5,5,"argentina");
		jugadores.agregarJugador(nuevo);
		jugadores.agregarJugador(nuevo1);
		jugadores.agregarJugador(nuevo2);
		
		assertTrue(jugadores.jugadoresEnPos("mirame").size()==2);
	}
	@Test
	public void MundialTraerJugadoresEnPosNoTienetest() {
		JugadoresDelMundial jugadores=new JugadoresDelMundial();
		DatoJugador nuevo=new DatoJugador("primero","defensor izquierdo","defensor derecho",8.3,5,5,5,"argentina");
		DatoJugador nuevo1=new DatoJugador("segundo","mirame","defensor derecho",8.3,5,5,5,"argentina");
		DatoJugador nuevo2=new DatoJugador("tercero","mirame","defensor derecho",8.3,5,5,5,"argentina");
		jugadores.agregarJugador(nuevo);
		jugadores.agregarJugador(nuevo1);
		jugadores.agregarJugador(nuevo2);
		
		assertTrue(!jugadores.jugadoresEnPos("mirame").contains(nuevo));
	}
}
