package Jugadores;

import java.util.ArrayList;

public class JugadoresDelMundial {
	public ArrayList<DatoJugador>jugadoresTotales;
	
	public JugadoresDelMundial(){
		jugadoresTotales=new ArrayList<DatoJugador>();

	}
	//agrega jugador en totales
	public boolean agregarJugador(DatoJugador a){
		for(DatoJugador jug:jugadoresTotales)
			if(jug.getNombre().toLowerCase().equals(a.getNombre().toLowerCase())&&
					jug.getPaisOrigen().toLowerCase().equals(a.getPaisOrigen().toLowerCase()))return false;
		
		jugadoresTotales.add(a);
		return true;
	}
	//devuelve la cant de jugadores totales;
	public int cantJugadores(){
		return jugadoresTotales.size();
	}
	//trae un jugador en la posicion correcta
	public DatoJugador getJugador(int i){
		if(i<0 || i>=cantJugadores())throw new IllegalArgumentException();
		return jugadoresTotales.get(i);
	}
	//devuelve una array con todos los jugadores en esa posicon
	 public ArrayList<DatoJugador> jugadoresEnPos(String posicion){
		return arregloConPos(posicion);
		
	}
	//verifica quien cumple las condiciones y lo agrega al arreglo
	private ArrayList<DatoJugador> arregloConPos(String posicion) {
		ArrayList<DatoJugador>nuevo=new ArrayList<DatoJugador>();
		
		for(int i=0;i<cantJugadores();i++){
			if(jugadoresTotales.get(i).getPosicionEnCancha()[0].equals(posicion)){
				nuevo.add(jugadoresTotales.get(i));}
			else {if(jugadoresTotales.get(i).getPosicionEnCancha().length==2 && 
					jugadoresTotales.get(i).getPosicionEnCancha()[1]==posicion){
					nuevo.add(jugadoresTotales.get(i));}
		}}
		
		return nuevo;
	}
	
	 public String[] getPosiciones(){
		 String[]posicion={"arquero","lateral izquierdo","lateral derecho",
					"central izquierdo","central derecho","volante izquierdo","volante derecho",
					"volante central","puntero izquierdo","puntero derecho","centro delantero"};
		 return posicion;
	 }
	 
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((jugadoresTotales == null) ? 0 : jugadoresTotales.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		JugadoresDelMundial other = (JugadoresDelMundial) obj;
		if (jugadoresTotales == null) {
			if (other.jugadoresTotales != null)
				return false;
		} else if (!jugadoresTotales.equals(other.jugadoresTotales))
			return false;
		return true;
	}
	
    public ArrayList<DatoJugador> getJugadoresTotales() {
		
		return jugadoresTotales;
	}
	

	
}
