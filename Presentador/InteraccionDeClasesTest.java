package Presentador;

import static org.junit.Assert.*;

import org.junit.Test;

import Jugadores.DatoJugador;

public class InteraccionDeClasesTest {

	@Test
	public void agregarJugadorTest() {
		InteraccionDeClases interaccion=new InteraccionDeClases();
		interaccion.agregarJugador("Pepe", "arquero", "central derecho", 9.7, 7, 0, 4,"Italia");
		DatoJugador nuevo =new DatoJugador("Pepe", "arquero", "central derecho", 9.7, 7, 0, 4,"Italia");
		assertTrue(interaccion.getJugadores().getJugadoresTotales().contains(nuevo));
		
	}
	
	@Test
	public void agregarJugadorNombreParecidoTest() {
		InteraccionDeClases interaccion=new InteraccionDeClases();
		interaccion.agregarJugador("Pepe", "arquero", "central derecho", 9.7, 7, 0, 4,"Italia");
		DatoJugador nuevo =new DatoJugador("pepe", "arquero", "central derecho", 9.7, 7, 0, 4,"Italia");
		assertFalse(interaccion.getJugadores().getJugadoresTotales().contains(nuevo));
		
		
	}
	
	@Test
	public void agregarJugadorNacionalidadDistintaTest() {
		InteraccionDeClases interaccion=new InteraccionDeClases();
		interaccion.agregarJugador("Pepe", "arquero", "central derecho", 9.7, 7, 0, 4,"Italia");
		DatoJugador nuevo =new DatoJugador("Pepe", "arquero", "central derecho", 9.7, 7, 0, 4,"Argentina");
		assertFalse(interaccion.getJugadores().getJugadoresTotales().contains(nuevo));
		
		
	}
	@Test
	public void traerDatosJugadoresTest(){
		InteraccionDeClases interaccion=new InteraccionDeClases();
		interaccion.traerDatosJugadores();
		DatoJugador nuevo =new DatoJugador("Pepe", "arquero", "central derecho", 9.7, 7, 0, 4,"Italia");
		//nuevo esta ingresado en los archivos
		assertTrue(interaccion.getJugadores().getJugadoresTotales().contains(nuevo));
	}
	
	@Test
	public void traerDatosJugadoresFalseTest(){
		InteraccionDeClases interaccion=new InteraccionDeClases();
		interaccion.traerDatosJugadores();
		//nuevo no esta ingresado
		DatoJugador nuevo =new DatoJugador("Pepe2", "arquero", "central derecho", 9.7, 7, 0, 4,"Italia");
		assertFalse(interaccion.getJugadores().getJugadoresTotales().contains(nuevo));
	}
	
	
	

}
