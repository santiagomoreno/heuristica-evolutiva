package Presentador;

import Archivos.ManejoDeArchivos;
import Comparar.Generador;
import Comparar.GeneradorAleatorio;
import Comparar.Implementar;
import Comparar.SolverGoloso;
import Jugadores.DatoJugador;
import Jugadores.JugadoresDelMundial;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;


public class InteraccionDeClases {

	private ManejoDeArchivos archivoTxt; 
	private SolverGoloso buscadorEquipoIdeal;
	private JugadoresDelMundial jugadores;
	
		
	public InteraccionDeClases(){
		archivoTxt=new ManejoDeArchivos();
		jugadores=new JugadoresDelMundial();
		traerDatosJugadores();
	}
	
	public void agregarJugador(String nombre
			,String posCancha, String posCancha1, double puntaje,
			int goles, int tarjetas, int faltas,String paisOrigen ){
		
		DatoJugador nuevo=new DatoJugador( nombre,posCancha,  posCancha1,  puntaje,
				 goles,  tarjetas,  faltas,paisOrigen);
		if(jugadores.agregarJugador(nuevo)==true){
			jugadores.agregarJugador(nuevo);
			escribirEnArchJug(nombre+","+posCancha+","+posCancha1+","+puntaje+","+goles+","+tarjetas+","+faltas+","+paisOrigen);
		}
	}
	
	public void traerDatosJugadores(){
		
		int aux=archivoTxt.sizeTxt(archivoTxt.getArchivoTodosJugadores());
			
		if(aux>0){
			for(int i=0;i<aux;i++){
				String[]valores=archivoTxt.traerDatos(i,archivoTxt.getArchivoTodosJugadores());
				DatoJugador nuevo=new DatoJugador(valores[0],valores[1],valores[2],Double.parseDouble(valores[3]),Integer.parseInt(valores[4]),
						Integer.parseInt(valores[5]),Integer.parseInt(valores[6]),valores[7]);
				
				jugadores.agregarJugador(nuevo);
			}
		}
	}
	
	public ArrayList<DatoJugador> dameEquipoIdeal(){
		
		//tengo q tomar la lista
		
		buscadorEquipoIdeal=new SolverGoloso(jugadores);
		Generador generador=new GeneradorAleatorio();
		Implementar maneraDeComparar=new Implementar();
			
		ArrayList<DatoJugador>res=buscadorEquipoIdeal.resolver(maneraDeComparar,generador); 
		
		guardarInformacionDeEquipo(res);
		
		return res;
	}
	
	private void escribirEnArcIdeal(String a){
		archivoTxt.escribirArchivo(a,archivoTxt.getArchivoEquipoIdeal());
	}
	private void escribirEnArchJug(String a){
		archivoTxt.escribirArchivo(a,archivoTxt.getArchivoTodosJugadores());
	}
	
	private void guardarInformacionDeEquipo(ArrayList<DatoJugador>equipo){
		
		escribirEnArcIdeal("");
		escribirEnArcIdeal("Equipo ideal hasta la fecha:");
		escribirEnArcIdeal(fechaActual());
		escribirEnArcIdeal("");

		double puntajePromedio=0;
		for(int i=0;i<equipo.size();i++){
			escribirEnArcIdeal(equipo.get(i).getPosicionEnCancha()[0]+". "+equipo.get(i).getNombre()+": pais origen ("+
			equipo.get(i).getPaisOrigen().toLowerCase()+") , promedio en torneo ("+equipo.get(i).getPuntajePromedio()+
			"), cantidad de goles ("+equipo.get(i).getGoles()+") , cantidad de amarillas ("+
			equipo.get(i).getCantTarjetas()+") , cantidad de faltas totales ("+equipo.get(i).getCantFaltas()+") ");
			puntajePromedio+=equipo.get(i).getPuntajePromedio();
		}
		puntajePromedio=puntajePromedio/equipo.size();
		escribirEnArcIdeal("");
		escribirEnArcIdeal("Puntaje promedio del equipo: "+puntajePromedio);
		escribirEnArcIdeal("");
		escribirEnArcIdeal("*****************");
		
	}
	
	private String fechaActual() {
		Date now = new Date(System.currentTimeMillis());
		SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat hour = new SimpleDateFormat("HH:mm:ss");

		return date.format(now)+" "+hour.format(now);
		
	}

	public String[] getPosicionesJugador() {
		return jugadores.getPosiciones();
	}

	public ArrayList<DatoJugador>jugadoresTotales(){
		ArrayList<DatoJugador>jugador=new ArrayList<DatoJugador>();
		for(int i=0;i<jugadores.cantJugadores();i++)
			jugador.add(jugadores.getJugador(i));
		return jugador;
	}
	
	public JugadoresDelMundial getJugadores() {
	
		return jugadores;
	}
	
	
//	public static void main(String[] args) {
//		InteraccionDeClases nuevo=new InteraccionDeClases();
//		nuevo.agregarJugador("armani", "arquero", "central derecho", 9.7, 7, 0, 4,"Argentina");
//		nuevo.agregarJugador("fazio", "lateral derecho", "central derecho", 9.11, 7, 0, 4,"Brasil");
//		nuevo.agregarJugador("mayada", "central derecho", "lateral derecho", 5.4, 7, 0, 4,"Italia");
//		nuevo.agregarJugador("mas", "central izquierdo", "lateral izquierdo", 9.7, 7, 0, 4,"Francia");
//		nuevo.agregarJugador("mascherano", "lateral izquierdo", "central Izquierdo", 9.11, 7, 0, 4,"Alemania");
//		nuevo.agregarJugador("lo celso", "volante derecho", "volante izquierdo", 5.4, 7, 0, 4,"Portugal");
//		nuevo.agregarJugador("pavon", "volante izquierdo", "volante derecho", 9.7, 7, 0, 4,"Espa�a");
//		nuevo.agregarJugador("ronaldo", "volante central", "centro delantero", 9.11, 7, 0, 4,"Uruguay");
//		nuevo.agregarJugador("rossi", "puntero derecho", "puntero izquierdo", 5.4, 7, 0, 4,"Inglaterra");
//		nuevo.agregarJugador("messi", "puntero izquierdo", "volante central", 9.7, 7, 0, 4,"Argentina");
//	nuevo.agregarJugador("del piero", "centro delantero", "puntero derecho", 9.11, 7, 0, 4,"Ecuador");
//		
//		
//		
//		
//	}
	

}
