package Archivos;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;

import org.junit.Test;

public class ManejoDeArchivosTest {

	@Test
	public void VerificarSizetest() throws IOException {
		File nuevo=new File("prueba1.txt");
		nuevo.createNewFile();
		ManejoDeArchivos manejo=new ManejoDeArchivos();
		if(manejo.sizeTxt(nuevo.getName())!=1)
			manejo.escribirArchivo("no hay copy past", nuevo.getName());
		assertTrue(manejo.sizeTxt(nuevo.getName())==1);
	}
	
	@Test
	public void VerSizeVaciotest() throws IOException {
		File nuevo=new File("prueba2.txt");
		nuevo.createNewFile();
		ManejoDeArchivos manejo=new ManejoDeArchivos();
		assertTrue(manejo.sizeTxt(nuevo.getName())==0);
	}
	
	@Test
	public void VerificarSizeGrandetest() throws IOException {
		File nuevo=new File("prueba3.txt");
		nuevo.createNewFile();
		ManejoDeArchivos manejo=new ManejoDeArchivos();
		if(manejo.sizeTxt(nuevo.getName())<4){
			boolean aux=true;
			while(aux){
				manejo.escribirArchivo("no hay copy", nuevo.getName());
				if(manejo.sizeTxt(nuevo.getName())==4)aux=false;
			}		
		}
		assertTrue(manejo.sizeTxt(nuevo.getName())==4);
	}
	@Test
	public void TraerDatosCorrectostest() throws IOException {
		File nuevo=new File("prueba4.txt");
		nuevo.createNewFile();
		ManejoDeArchivos manejo=new ManejoDeArchivos();
		String cadena="no hay copy past";
		if(manejo.sizeTxt(nuevo.getName())<1)
			manejo.escribirArchivo(cadena, nuevo.getName());

		assertTrue(manejo.traerDatos(0, nuevo.getName())[0].equals(cadena));
	}
	@Test
	public void TraerDatosInCorrectostest() throws IOException {
		File nuevo=new File("prueba5.txt");
		nuevo.createNewFile();
		ManejoDeArchivos manejo=new ManejoDeArchivos();
		String cadena="no hay ,copy past";
		if(manejo.sizeTxt(nuevo.getName())<1)
			manejo.escribirArchivo(cadena, nuevo.getName());

		assertFalse(manejo.traerDatos(0, nuevo.getName())[0].equals("no hay copy past"));
	}
	@Test
	public void TraerDatoscorrectosEspecificostest() throws IOException {
		File nuevo=new File("prueba6.txt");
		nuevo.createNewFile();
		ManejoDeArchivos manejo=new ManejoDeArchivos();
		String cadena="no hay,copy";
		if(manejo.sizeTxt(nuevo.getName())<4){
			boolean aux=true;
			while(aux){
				manejo.escribirArchivo(cadena, nuevo.getName());
				if(manejo.sizeTxt(nuevo.getName())==4)aux=false;
			}		
		}
		assertTrue(cadena.split(",")[1].equals(manejo.traerDatos(3,nuevo.getName())[1]));
	}
	
}
