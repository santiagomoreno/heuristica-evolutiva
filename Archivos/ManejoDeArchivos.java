package Archivos;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;


public class ManejoDeArchivos {

	public void escribirArchivo(String nuevo,String archi){
		 try(BufferedReader br=new BufferedReader(new FileReader(archi));
		            BufferedWriter bw=new BufferedWriter(new FileWriter(archi,true));){
			 int cont=0;
			 
			 while(br.readLine()!= null || cont==0 ){
				 if(br.readLine()==null){
					 bw.write(nuevo);
					 bw.newLine();
					 cont++;
				 }
			 }
		      //Guardamos los cambios del fichero
		     bw.flush();
		     //Leemos el fichero y lo mostramos por pantalla
		            String linea=br.readLine();
		            while(linea!=null){
		                System.out.println(linea);
		                linea=br.readLine();
		            }
		        }catch(IOException e){
		            System.out.println("Error E/S: "+e);
		        }
		    }
	

	public int sizeTxt(String archi){
		
		File archivo = null;
	      FileReader fr = null;
	      BufferedReader br = null;

	      try {
	         // Apertura del fichero y creacion de BufferedReader para poder
	         // hacer una lectura comoda (disponer del metodo readLine()).
	    	  String txt=archi;
	    	
	         archivo = new File (txt);
	         fr = new FileReader (archivo);
	         br = new BufferedReader(fr);

	         // Lectura del fichero
	         @SuppressWarnings("unused")
			String linea;
			 int contador=0;
			 while((linea=br.readLine())!=null){
				 contador++;
			 }
			return contador;
	      }
	      catch(Exception e){
	         e.printStackTrace();
	      }finally{
	         // En el finally cerramos el fichero, para asegurarnos
	         // que se cierra tanto si todo va bien como si salta 
	         // una excepcion.
	         try{                    
	            if( null != fr ){   
	               fr.close();     
	            }                  
	         }catch (Exception e2){ 
	            e2.printStackTrace();
	         }
	      }
		return -1;
	   
	}
	
	@SuppressWarnings("resource")
	public String[] traerDatos(int posicion,String archi) {
		
		File archivo = null;
	      FileReader fr = null;
	      BufferedReader br = null;

	      try {
	         // Apertura del fichero y creacion de BufferedReader para poder
	         // hacer una lectura comoda (disponer del metodo readLine()).
	    	 String txt=archi;
	    	  
	         archivo = new File (txt);
	         fr = new FileReader (archivo);
	         br = new BufferedReader(fr);

	         // Lectura del fichero
	         if(posicion>=0 && posicion<sizeTxt(archi)){
		         String linea;
				 int contador=0;
				 while((linea=br.readLine())!=null){
				    if(contador==posicion){
				    	return linea.split(",");
				    }
					 contador++;
				 }
			 }
			return null;
	      }
	      catch(Exception e){
	         e.printStackTrace();
	      }finally{
	         // En el finally cerramos el fichero, para asegurarnos
	         // que se cierra tanto si todo va bien como si salta 
	         // una excepcion.
	         try{                    
	            if( null != fr ){   
	               fr.close();     
	            }                  
	         }catch (Exception e2){ 
	            e2.printStackTrace();
	         }
	      }
		return null;
	}
	public String getArchivoEquipoIdeal(){
		return "equiposIdeales.txt";
	}
	public String getArchivoTodosJugadores(){
		return "datosJugadores.txt";
	}
	public static void main(String[] args) {
		
	}
	/*
	@SuppressWarnings("resource")
	public String traerLinea(int posicion) {
		
		File archivo = null;
	      FileReader fr = null;
	      BufferedReader br = null;

	      try {
	         // Apertura del fichero y creacion de BufferedReader para poder
	         // hacer una lectura comoda (disponer del metodo readLine()).
	    	 String txt=getArchivo();
	    	  
	         archivo = new File (txt);
	         fr = new FileReader (archivo);
	         br = new BufferedReader(fr);

	         // Lectura del fichero
	         if(posicion>=0 && posicion<sizeTxt("equipoIdeal")){
		         String linea;
				 int contador=0;
				 while((linea=br.readLine())!=null){
				    if(contador==posicion) return linea;
					 contador++;
				 }
			 }
			return null;
	      }
	      catch(Exception e){
	         e.printStackTrace();
	      }finally{
	         // En el finally cerramos el fichero, para asegurarnos
	         // que se cierra tanto si todo va bien como si salta 
	         // una excepcion.
	         try{                    
	            if( null != fr ){   
	               fr.close();     
	            }                  
	         }catch (Exception e2){ 
	            e2.printStackTrace();
	         }
	      }
		return null;
	}
	
	@SuppressWarnings("resource")
	public String traerLinea2(int posicion) {
		
		File archivo = null;
	      FileReader fr = null;
	      BufferedReader br = null;

	      try {
	         // Apertura del fichero y creacion de BufferedReader para poder
	         // hacer una lectura comoda (disponer del metodo readLine()).
	    	 String txt=getArchivoDatos();
	    	  
	         archivo = new File (txt);
	         fr = new FileReader (archivo);
	         br = new BufferedReader(fr);

	         // Lectura del fichero
	         if(posicion>=0 && posicion<sizeTxt("datosJugadores")){
		         String linea;
				 int contador=0;
				 while((linea=br.readLine())!=null){
				    if(contador==posicion) return linea;
					 contador++;
				 }
			 }
			return null;
	      }
	      catch(Exception e){
	         e.printStackTrace();
	      }finally{
	         // En el finally cerramos el fichero, para asegurarnos
	         // que se cierra tanto si todo va bien como si salta 
	         // una excepcion.
	         try{                    
	            if( null != fr ){   
	               fr.close();     
	            }                  
	         }catch (Exception e2){ 
	            e2.printStackTrace();
	         }
	      }
		return null;
	}
	
	
	
	
	public boolean estaJugador(String jugador){
		
		
//		for(int i=0;i>sizeTxt("datosJugadores");i++){
		int i=0;
		//System.out.println(traerLinea2(i)+"aklsdjlkasjdlkasjldfkaslfkjhasdjkl");
		while(traerLinea2(i)!=null){	
		    if(traerLinea2(i)!=null && traerLinea2(i).equals(jugador)){
			    return true;		
		    }  
		    i++;
		   }
		return false;
	}
	
	*/
	
	
}
