package Comparar;

import java.util.ArrayList;
import java.util.Comparator;

import Jugadores.DatoJugador;

public interface Comparador extends Comparator<DatoJugador> {
	public int compare(DatoJugador uno, DatoJugador dos);
	public int cantMaxJugConTarjeta();
	public int cantMinJugGoles();
	public int cantMaxNacion();

	public void verificarElPaisOrigen(ArrayList<DatoJugador> candidatos, ArrayList<DatoJugador>equipoIdeal);
	public void verifCantAmarillas(ArrayList<DatoJugador> candidatos, ArrayList<DatoJugador>equipoIdeal);
	public void verifCantGoles(ArrayList<DatoJugador> candidatos, ArrayList<DatoJugador>equipoIdeal);
	
	

}