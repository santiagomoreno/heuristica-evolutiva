package Comparar;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

import Jugadores.DatoJugador;
import Jugadores.JugadoresDelMundial;

public class SolverGoloso{
	
	// Instancia a resolver
	private JugadoresDelMundial _instancia;
	private ArrayList<DatoJugador>equipoIdeal;
	
	
	public SolverGoloso(JugadoresDelMundial instancia)
	{
		_instancia = instancia;
		equipoIdeal = new  ArrayList<DatoJugador>();
		
	}
	
	// Heurística golosa
	public ArrayList<DatoJugador> resolver(Implementar comparado,Generador generador){
		
		ArrayList<DatoJugador> posible1=new ArrayList<>();
		ArrayList<DatoJugador> posible2=new ArrayList<>();
		ArrayList<DatoJugador> posible3=new ArrayList<>();
		
		
		
		for(int i=0;i<_instancia.getPosiciones().length;i++){
			
//trae en nueva lista todos los jugadores que tienen esa posicion(arquero,centro delantero,etc)
			ArrayList<DatoJugador> ordenados = obtenerJugadoresEnPos(_instancia.getPosiciones()[i],comparado);
			ArrayList<DatoJugador> ordenados2 = obtenerJugadoresEnPos(_instancia.getPosiciones()[_instancia.getPosiciones().length-1-i],comparado);
			
		
			comparado.verificarElPaisOrigen(ordenados,posible1);
			comparado.verificarElPaisOrigen(ordenados,posible2);
			comparado.verificarElPaisOrigen(ordenados2,posible3);
			
//se fija si no hay mas de 4 del mismo pais, solo los que estan ya agregados a equipo ideal
			comparado.verifCantAmarillas(ordenados,posible1);
			comparado.verifCantAmarillas(ordenados,posible2);
			comparado.verifCantAmarillas(ordenados2,posible3);
//verif cant jugadores con amarilla en equipo ideal			
			comparado.verifCantGoles(ordenados,posible1);
			comparado.verifCantGoles(ordenados,posible2);
			comparado.verifCantGoles(ordenados2,posible3);
//verif cant jugadores con goles	
			
			
			if(ordenados.size()==0){
				
				equipoIdeal.add(new DatoJugador("sin nombre",_instancia.getPosiciones()[i],
					"sin posicion",0.0,0,0,0,"sin pais"));}
			
			else {
				
				
				randomJugador(ordenados,posible1, generador);
				//desde el arquero
				mejorPromedio(ordenados,posible2);				
			}			
             if(ordenados2.size()==0){
				
				equipoIdeal.add(new DatoJugador("sin nombre",_instancia.getPosiciones()[i],
					"sin posicion",0.0,0,0,0,"sin pais"));}			
			else {
							
				//desde los delanteros
				mejorPromedio(ordenados2,posible3);

			}					
			
		}				
		if(promedioEquipo(posible1)>=promedioEquipo(posible2) && promedioEquipo(posible1)>=promedioEquipo(posible3)){
			equipoIdeal=posible1;
			return equipoIdeal;
		} 
		else{
			
			if(promedioEquipo(posible2)>=promedioEquipo(posible3)){
				equipoIdeal=posible2;
				return equipoIdeal;
			}
		}
		
		equipoIdeal=posible3;
		return equipoIdeal;
		
	}
	
	
	double promedioEquipo(ArrayList<DatoJugador> posible){
		double cont=0;
		
		for(int i=0;i<posible.size();i++){
			cont=cont+posible.get(i).getPuntajePromedio();
		}
		
		
		
		return cont/posible.size();
		
	}
	
	
	void mejorPromedio(ArrayList<DatoJugador> ordenados,
			ArrayList<DatoJugador> posible2) {		
		int j=0;
		while(j<ordenados.size() && j!=-1){
		    if(!existeJugador(ordenados.get(j))){		    	
		    	posible2.add(ordenados.get(0));
			      j=-2;
								
		}j++;}
		
		
		
	}

	 void randomJugador(ArrayList<DatoJugador> ordenados, ArrayList<DatoJugador> posible,Generador generador) {
				
		int j=0;
		while(j<ordenados.size() && j!=-1){
		    if(!existeJugador(ordenados.get(j))){
		    	
		    	
		    	int indR=generador.nextInt(ordenados.size());
		    	
			      posible.add(ordenados.get(indR));
			      j=-2;
								
		}j++;}
		
		
	}

	// Ordenamiento por coeficiente
	private ArrayList<DatoJugador>obtenerJugadoresEnPos(String pos,Implementar comparado){
		ArrayList<DatoJugador> ret = _instancia.jugadoresEnPos(pos);
		if(ret.size()>1)Collections.sort(ret, comparado);
		
		
		return ret;
	}		
		
	//agrega un jugador al equipo ideal	
	 void agregarJugadorEquipoIdeal(DatoJugador nuevo){
		equipoIdeal.add(nuevo);
				
	}
	 
	 public boolean existeJugador(DatoJugador datoJugador){
			return equipoIdeal.contains(datoJugador);
		}	
	 
}
