package Comparar;

import java.util.ArrayList;

import Jugadores.DatoJugador;

public class Implementar implements Comparador {
	

	@Override
	public int compare(DatoJugador uno, DatoJugador dos) {
		if(uno.obtenerCoeficiente()>dos.obtenerCoeficiente())return -1;
		if(uno.obtenerCoeficiente()==dos.obtenerCoeficiente())return 0;
		return 1;	
	}
	
	@Override
	public int cantMaxJugConTarjeta() {
		return 5;
	}
	
	@Override
	public int cantMaxNacion() {
		return 4;
	}
	
	@Override
	public int cantMinJugGoles() {
		return 5;
	}

	@Override
	public void verificarElPaisOrigen(ArrayList<DatoJugador> candidatos,ArrayList<DatoJugador> equipoIdeal) {
		if(!equipoIdeal.isEmpty() && !candidatos.isEmpty()){
		
			ArrayList<DatoJugador>aux=new ArrayList<DatoJugador>();
			for(DatoJugador jug:candidatos){
				if(cantJugadoresPais(jug.getPaisOrigen(),equipoIdeal)<cantMaxNacion()){
					aux.add(jug);
				}
			}
			candidatos.clear();
			for(DatoJugador jug:aux)candidatos.add(jug);		
		}		
	}

	private int cantJugadoresPais(String paisOrigen,ArrayList<DatoJugador> equipoIdeal) {
		int cont=0;
		
		for(DatoJugador jug:equipoIdeal){
			
			if(paisOrigen.toLowerCase().equals(jug.getPaisOrigen().toLowerCase()))cont+=1;
		}
		return cont;
	}

	@Override
	public void verifCantAmarillas(ArrayList<DatoJugador> candidatos,ArrayList<DatoJugador> equipoIdeal) {
		if(!equipoIdeal.isEmpty() && !candidatos.isEmpty()){
			ArrayList<DatoJugador>aux=new ArrayList<DatoJugador>();	
			
			if(cantAmarillasEquipo(equipoIdeal)>=cantMaxJugConTarjeta()){
				for(DatoJugador jug:candidatos){if(jug.getCantTarjetas()==0)aux.add(jug);}
				
				candidatos.clear();
				
			}
			
			for(DatoJugador jug:aux)candidatos.add(jug);	
		}
			
	}

	
	private int cantAmarillasEquipo(ArrayList<DatoJugador> equipoIdeal) {
		int cont=0;
		for(DatoJugador jug:equipoIdeal)if(jug.getCantTarjetas()>0)cont+=1;
		
		return cont;
	}

	@Override
	public void verifCantGoles(ArrayList<DatoJugador> candidatos,ArrayList<DatoJugador> equipoIdeal) {
		if(!equipoIdeal.isEmpty() && !candidatos.isEmpty()){
			if(equipoIdeal.size()>6){
				ArrayList<DatoJugador>aux=new ArrayList<DatoJugador>();							
				for(DatoJugador jug:candidatos){
					if(jug.getGoles()>0)aux.add(jug);
				}
				candidatos.clear();
				for(DatoJugador jug:aux)candidatos.add(jug);	
			}
		}		
	}
	
}