package Comparar;

import static org.junit.Assert.*;

import java.lang.reflect.Array;
import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import Archivos.ManejoDeArchivos;
import Jugadores.DatoJugador;
import Jugadores.JugadoresDelMundial;

public class SolverGolosoTest {
	
    ManejoDeArchivos archivoTxt; 
	JugadoresDelMundial jugadores;
	
	void inicializarJugadores10(){
		jugadores=new JugadoresDelMundial();
		jugadores.agregarJugador(new DatoJugador("jugador1", "arquero", "", 10, 7, 0, 4,"Argentina"));
		jugadores.agregarJugador(new DatoJugador("jugador2", "lateral izquierdo", "", 10, 7, 0, 4,"Brasil"));
		jugadores.agregarJugador(new DatoJugador("jugador3", "lateral derecho", "", 10, 7, 0, 4,"Italia"));
		jugadores.agregarJugador(new DatoJugador("jugador4", "central izquierdo", "", 10, 7, 0, 4,"Francia"));
		jugadores.agregarJugador(new DatoJugador("jugador5", "central derecho", "", 10, 7, 0, 4,"Alemania"));
		jugadores.agregarJugador(new DatoJugador("jugador6", "volante izquierdo", "", 10, 7, 0, 4,"Portugal"));
		jugadores.agregarJugador(new DatoJugador("jugador7", "volante derecho", "", 10, 7, 0, 4,"Espa�a"));
		jugadores.agregarJugador(new DatoJugador("jugador8", "volante central", "", 10, 7, 0, 4,"Uruguay"));
		jugadores.agregarJugador(new DatoJugador("jugador9", "puntero izquierdo", "", 10, 7, 0, 4,"Inglaterra"));
		jugadores.agregarJugador(new DatoJugador("jugador10", "puntero derecho", "", 10, 7, 0, 4,"Argentina"));
		jugadores.agregarJugador(new DatoJugador("jugador11", "centro delantero", "", 10, 7, 0, 4,"Ecuador"));
		
	}
	
	void inicializarJugadoresMuchasAmariilas(){
		jugadores=new JugadoresDelMundial();
		jugadores.agregarJugador(new DatoJugador("jugador1", "arquero", "", 10, 7, 1, 4,"Argentina"));
		jugadores.agregarJugador(new DatoJugador("jugador2", "lateral izquierdo", "", 10, 7, 1, 4,"Brasil"));
		jugadores.agregarJugador(new DatoJugador("jugador3", "lateral derecho", "", 10, 7, 1, 4,"Italia"));
		jugadores.agregarJugador(new DatoJugador("jugador4", "central izquierdo", "", 10, 7, 1, 4,"Francia"));
		jugadores.agregarJugador(new DatoJugador("jugador5", "central derecho", "", 10, 7, 1, 4,"Alemania"));
		jugadores.agregarJugador(new DatoJugador("jugador6", "volante izquierdo", "", 10, 7, 1, 4,"Portugal"));
		jugadores.agregarJugador(new DatoJugador("jugador7", "volante derecho", "", 10, 7, 1, 4,"Espa�a"));
		jugadores.agregarJugador(new DatoJugador("jugador8", "volante central", "", 10, 7, 1, 4,"Uruguay"));
		jugadores.agregarJugador(new DatoJugador("jugador9", "puntero izquierdo", "", 10, 7, 1, 4,"Inglaterra"));
		jugadores.agregarJugador(new DatoJugador("jugador10", "puntero derecho", "", 10, 7, 1, 4,"Argentina"));
		jugadores.agregarJugador(new DatoJugador("jugador11", "centro delantero", "", 10, 7, 1, 4,"Ecuador"));
		
	}
	
	
	void inicializarJugadoresMismaSeleccion(){
		jugadores=new JugadoresDelMundial();
		jugadores.agregarJugador(new DatoJugador("jugador1", "arquero", "", 10, 7, 0, 4,"Argentina"));
		jugadores.agregarJugador(new DatoJugador("jugador2", "lateral izquierdo", "", 10, 7, 0, 4,"Argentina"));
		jugadores.agregarJugador(new DatoJugador("jugador3", "lateral derecho", "", 10, 7, 0, 4,"Argentina"));
		jugadores.agregarJugador(new DatoJugador("jugador4", "central izquierdo", "", 10, 7, 0, 4,"Argentina"));
		jugadores.agregarJugador(new DatoJugador("jugador5", "central derecho", "", 10, 7, 0, 4,"Argentina"));
		jugadores.agregarJugador(new DatoJugador("jugador6", "volante izquierdo", "", 10, 7, 0, 4,"Argentina"));
		jugadores.agregarJugador(new DatoJugador("jugador7", "volante derecho", "", 10, 7, 0, 4,"Argentina"));
		jugadores.agregarJugador(new DatoJugador("jugador8", "volante central", "", 10, 7, 0, 4,"Argentina"));
		jugadores.agregarJugador(new DatoJugador("jugador9", "puntero izquierdo", "", 10, 7, 0, 4,"Argentina"));
		jugadores.agregarJugador(new DatoJugador("jugador10", "puntero derecho", "", 10, 7, 0, 4,"Argentina"));
		jugadores.agregarJugador(new DatoJugador("jugador11", "centro delantero", "", 10, 7, 0, 4,"Argentina"));
		
	}
	
	

	@Test
	public void resolverTest() {
		inicializarJugadores10();
		SolverGoloso nuevo=new SolverGoloso(jugadores);
		Implementar maneraDeComparar=new Implementar();
		Generador generador=new GeneradorPrefijado();
		assertEquals(jugadores.getJugadoresTotales(),nuevo.resolver(maneraDeComparar,generador));
		
		
		
	}
	
	@Test
	public void resolverTest2() {
		inicializarJugadores10();
		//Agrego un mejor arquero
		jugadores.agregarJugador(new DatoJugador("jugador12", "arquero", "", 12, 7, 0, 4,"Argentina"));
		SolverGoloso nuevo=new SolverGoloso(jugadores);
		Implementar maneraDeComparar=new Implementar();
		Generador generador=new GeneradorPrefijado();
		assertEquals("jugador12",nuevo.resolver(maneraDeComparar,generador).get(0).getNombre());
		
		
		
	}
	
	
	
	
	@Test
	public void equipoMaxAmarillasTest() {
		inicializarJugadoresMuchasAmariilas();
		SolverGoloso nuevo=new SolverGoloso(jugadores);
		Implementar maneraDeComparar=new Implementar();
		Generador generador=new GeneradorPrefijado();
		assertEquals(5,nuevo.resolver(maneraDeComparar,generador).size());
		
		
		
	}
	
	@Test
	public void equipoMaxMismoPaisTest() {
		inicializarJugadoresMismaSeleccion();
		SolverGoloso nuevo=new SolverGoloso(jugadores);
		Implementar maneraDeComparar=new Implementar();
		Generador generador=new GeneradorPrefijado();		
		assertEquals(4,nuevo.resolver(maneraDeComparar,generador).size());
				
	}
	
	@Test
	public void mejorPromedioTest(){
		
		ArrayList<DatoJugador> posibles=new ArrayList<>();
		ArrayList<DatoJugador> ordenados=new ArrayList<>();
		SolverGoloso nuevo=new SolverGoloso(jugadores);
		//Jugadores ordenados por el promedio
		ordenados.add(new DatoJugador("jugador1", "lateral izquierdo", "", 10, 7, 0, 4,"Argentina"));
		ordenados.add(new DatoJugador("jugador2", "lateral izquierdo", "", 9, 7, 0, 4,"Argentina"));
		ordenados.add(new DatoJugador("jugador3", "lateral izquierdo", "", 8, 7, 0, 4,"Argentina"));
		nuevo.mejorPromedio(ordenados, posibles);
		assertEquals(ordenados.get(0),posibles.get(0));
				
		
	}
	
	
	@Test
	public void ordenadosVacioTest(){
		
		ArrayList<DatoJugador> posibles=new ArrayList<>();
		ArrayList<DatoJugador> ordenados=new ArrayList<>();
		SolverGoloso nuevo=new SolverGoloso(jugadores);
		//Jugadores ordenados por el promedio
		
		nuevo.mejorPromedio(ordenados, posibles);
		assertTrue(posibles.size()==0);
				
		
	}
	
	
	
    @Test	
    public void randomJugadorTest(){
		
		ArrayList<DatoJugador> posibles=new ArrayList<>();
		ArrayList<DatoJugador> ordenados=new ArrayList<>();
		SolverGoloso nuevo=new SolverGoloso(jugadores);
		//Jugadores ordenados por el promedio
		ordenados.add(new DatoJugador("jugador1", "lateral izquierdo", "", 10, 7, 0, 4,"Argentina"));
		ordenados.add(new DatoJugador("jugador2", "lateral izquierdo", "", 9, 7, 0, 4,"Argentina"));
		ordenados.add(new DatoJugador("jugador3", "lateral izquierdo", "", 8, 7, 0, 4,"Argentina"));
		Generador generador=new GeneradorPrefijado();
		//random prefijado next int devuelve siempre cero
		nuevo.randomJugador(ordenados, posibles,generador);		
		assertEquals(ordenados.get(0),posibles.get(0));
						
	}
	

}
